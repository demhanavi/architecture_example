package com.example.architectureexample.persistence.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "example_entity")
public class ExampleEntity {

    @PrimaryKey
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
