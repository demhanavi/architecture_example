package com.example.architectureexample.application.builder;

import com.example.architectureexample.api.ExampleApi;
import com.example.architectureexample.application.builder.modules.AppContextModule;
import com.example.architectureexample.application.builder.modules.NetworkModule;
import com.example.architectureexample.application.builder.modules.RxModule;
import com.example.architectureexample.application.builder.modules.apimodules.ExampleApiServiceModule;
import com.example.architectureexample.persistence.AppDatabase;
import com.example.architectureexample.util.PropertiesReader;
import com.example.architectureexample.util.rx.RxSchedulers;

import dagger.Component;

@AppScope
@Component(modules = {AppContextModule.class, RxModule.class, NetworkModule.class, ExampleApiServiceModule.class})
public interface AppComponent {
    RxSchedulers rxSchedulers();
    AppDatabase appDatabase();
    ExampleApi exampleApiService();
    PropertiesReader propertiesReader();
}
