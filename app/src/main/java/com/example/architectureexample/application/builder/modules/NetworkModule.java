package com.example.architectureexample.application.builder.modules;

import android.content.Context;

import com.example.architectureexample.BuildConfig;
import com.example.architectureexample.application.builder.AppScope;
import com.example.architectureexample.util.PropertiesReader;
import com.example.architectureexample.util.rx.AppRxSchedulers;

import java.io.File;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @AppScope
    @Provides
    OkHttpClient provideClient(PropertiesReader propertiesReader, HttpLoggingInterceptor logger, Cache cache){
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        clientBuilder.connectTimeout(propertiesReader.getRequestTimeout(), TimeUnit.MILLISECONDS);
        clientBuilder.readTimeout(propertiesReader.getRequestTimeout(), TimeUnit.MILLISECONDS);
        clientBuilder.writeTimeout(propertiesReader.getRequestTimeout(), TimeUnit.MILLISECONDS);
        if(BuildConfig.DEBUG){
            clientBuilder.addInterceptor(logger);
        }
        clientBuilder.cache(cache);
        return clientBuilder.build();
    }

    @AppScope
    @Provides
    HttpLoggingInterceptor provideLogger(){
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.level(HttpLoggingInterceptor.Level.BODY);
        return logger;
    }

    @AppScope
    @Provides
    Cache provideCache(File file){
        return new Cache(file, 10 * 10 * 1000);
    }

    @AppScope
    @Provides
    File provideCacheFile(Context context){
        return context.getFilesDir();
    }

    @AppScope
    @Provides
    RxJava2CallAdapterFactory provideRxAdapter(){
        return RxJava2CallAdapterFactory.createWithScheduler(AppRxSchedulers.INTERNET_SCHEDULERS);
    }

    @AppScope
    @Provides
    GsonConverterFactory provideConverterFactory(){
        return GsonConverterFactory.create();
    }
}
