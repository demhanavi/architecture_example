package com.example.architectureexample.application.builder.modules;

import com.example.architectureexample.application.builder.AppScope;
import com.example.architectureexample.util.rx.AppRxSchedulers;
import com.example.architectureexample.util.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class RxModule {

    @AppScope
    @Provides
    RxSchedulers provideRxSchedulers() {
        return new AppRxSchedulers();
    }
}
