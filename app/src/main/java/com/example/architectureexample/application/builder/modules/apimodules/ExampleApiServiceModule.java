package com.example.architectureexample.application.builder.modules.apimodules;

import com.example.architectureexample.api.ExampleApi;
import com.example.architectureexample.application.builder.AppScope;
import com.example.architectureexample.util.PropertiesReader;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ExampleApiServiceModule {

    @AppScope
    @Provides
    ExampleApi provideExampleApi(PropertiesReader propertiesReader, OkHttpClient client, GsonConverterFactory converterFactory, RxJava2CallAdapterFactory rxAdapter){
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(propertiesReader.getBaseUrl())
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(rxAdapter)
                .build();
        return retrofit.create(ExampleApi.class);
    }
}
