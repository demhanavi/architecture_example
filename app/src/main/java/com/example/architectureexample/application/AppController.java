package com.example.architectureexample.application;

import android.app.Application;

import com.example.architectureexample.application.builder.AppComponent;
import com.example.architectureexample.application.builder.DaggerAppComponent;
import com.example.architectureexample.application.builder.modules.AppContextModule;

public class AppController extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initAppComponent();
    }

    private void initAppComponent(){
        appComponent = DaggerAppComponent.builder().appContextModule(new AppContextModule(this)).build();
    }

    public static AppComponent getAppComponent(){
        return appComponent;
    }
}
