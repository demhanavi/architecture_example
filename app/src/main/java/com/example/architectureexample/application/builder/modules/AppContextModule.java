package com.example.architectureexample.application.builder.modules;

import android.content.Context;

import com.example.architectureexample.application.builder.AppScope;
import com.example.architectureexample.persistence.AppDatabase;
import com.example.architectureexample.util.PropertiesReader;

import dagger.Module;
import dagger.Provides;

@Module
public class AppContextModule {

    private final Context context;

    public AppContextModule(Context context){
        this.context = context;
    }

    @AppScope
    @Provides
    Context provideAppContext(){
        return context;
    }

    @AppScope
    @Provides
    AppDatabase provideAppDatabase(){
        return AppDatabase.getInstance(context);
    }

    @AppScope
    @Provides
    PropertiesReader providePropertiesReader(){
        return new PropertiesReader(context);
    }
}
