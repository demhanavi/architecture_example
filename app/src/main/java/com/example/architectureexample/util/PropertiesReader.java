package com.example.architectureexample.util;

import android.content.Context;

import com.example.architectureexample.BuildConfig;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

    private static final String appFilename = "app.properties";

    private Context context;
    private Properties appProperties;

    public PropertiesReader(Context context) {
        this.context = context;
        initProperties();
    }

    private void initProperties() {
        appProperties = new Properties();
        try {
            InputStream propertiesStream = context.getAssets().open(appFilename);
            appProperties.load(propertiesStream);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private Object getProperty(String propertyName) {
        return appProperties.getProperty(propertyName);
    }

    private String getBuildType(){
        return BuildConfig.BUILD_TYPE;
}

    public long getRequestTimeout() {
        return Long.parseLong(getProperty("app.request.timeout").toString());
    }

    public String getBaseUrl() {
        return getProperty("app.url.base." + getBuildType()).toString();
    }
}
