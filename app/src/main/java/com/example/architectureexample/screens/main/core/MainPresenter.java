package com.example.architectureexample.screens.main.core;

import android.view.View;

import com.example.architectureexample.R;
import com.example.architectureexample.api.dto.PokemonListResponse;
import com.example.architectureexample.screens.main.MainActivity;
import com.example.architectureexample.util.rx.RxSchedulers;

import java.util.Collections;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class MainPresenter {

    private MainActivity context;
    private MainView view;
    private MainModel model;
    private RxSchedulers rxSchedulers;
    private CompositeDisposable subscriptions = new CompositeDisposable();

    public MainPresenter(MainActivity context, MainView view, MainModel model, RxSchedulers rxSchedulers) {
        this.context = context;
        this.view = view;
        this.model = model;
        this.rxSchedulers = rxSchedulers;
    }

    public void onCreate(){
        subscriptions.add(handleClicks());
    }

    private Disposable handleClicks(){
        return view.getClickSubjects().subscribe(viewId -> {
            if(viewId == R.id.get_photos_button){
                view.disableComponents();
                getPokemon();
            }
        });
    }

    private void getPokemon(){
        Disposable getPokemon = model.getPokemon(1)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.androidThread())
                .subscribe(this::onGetPokemon, this::onErrorGetPokemon);
        subscriptions.add(getPokemon);
    }

    private void onErrorGetPokemon(Throwable error) {
        view.enableComponents();
        view.showToast("consumo incorrecto");
        error.printStackTrace();
    }

    private void onGetPokemon(PokemonListResponse response) {
        view.enableComponents();
        view.showToast("Consumo correcto");
        Collections.reverse(response.getPokemon());
        view.fillPokemonList(response.getPokemon());
    }

    public View getView(){
        return view.getView();
    }
}
