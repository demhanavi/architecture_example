package com.example.architectureexample.screens.main.dagger;

import com.example.architectureexample.application.builder.AppComponent;
import com.example.architectureexample.screens.main.MainActivity;

import dagger.Component;

@MainScope
@Component(modules = {MainModule.class}, dependencies = {AppComponent.class})
public interface MainComponent {
    void inject(MainActivity context);
}
