package com.example.architectureexample.screens.main.dagger;

import com.example.architectureexample.api.ExampleApi;
import com.example.architectureexample.persistence.AppDatabase;
import com.example.architectureexample.screens.main.MainActivity;
import com.example.architectureexample.screens.main.core.MainModel;
import com.example.architectureexample.screens.main.core.MainPresenter;
import com.example.architectureexample.screens.main.core.MainView;
import com.example.architectureexample.util.PropertiesReader;
import com.example.architectureexample.util.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {

    private MainActivity context;

    public MainModule(MainActivity context){
        this.context = context;
    }

    @MainScope
    @Provides
    MainActivity provideContext(){
        return context;
    }

    @MainScope
    @Provides
    MainModel provideModel(ExampleApi api, PropertiesReader propertiesReader, AppDatabase appDatabase, RxSchedulers schedulers){
        return new MainModel(api, propertiesReader, appDatabase, schedulers);
    }

    @MainScope
    @Provides
    MainView provideView(){
        return new MainView(context);
    }

    @MainScope
    @Provides
    MainPresenter providePresenter(MainActivity context, MainView view, MainModel model, RxSchedulers rxSchedulers){
        return new MainPresenter(context, view, model, rxSchedulers);
    }
}
