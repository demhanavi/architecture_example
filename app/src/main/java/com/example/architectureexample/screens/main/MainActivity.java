package com.example.architectureexample.screens.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.architectureexample.R;
import com.example.architectureexample.application.AppController;
import com.example.architectureexample.screens.main.core.MainModel;
import com.example.architectureexample.screens.main.core.MainPresenter;
import com.example.architectureexample.screens.main.core.MainView;
import com.example.architectureexample.screens.main.dagger.DaggerMainComponent;
import com.example.architectureexample.screens.main.dagger.MainModule;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject MainModel model;
    @Inject MainView view;
    @Inject MainPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerMainComponent.builder()
                .appComponent(AppController.getAppComponent())
                .mainModule(new MainModule(this))
                .build()
                .inject(this);
        setContentView(presenter.getView());
        presenter.onCreate();
    }
}
