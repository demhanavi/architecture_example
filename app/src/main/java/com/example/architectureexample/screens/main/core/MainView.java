package com.example.architectureexample.screens.main.core;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.architectureexample.R;
import com.example.architectureexample.api.dto.PokemonSimpleObject;
import com.example.architectureexample.screens.main.MainActivity;
import com.example.architectureexample.screens.main.adapter.PokemonNameAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class MainView {

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.recycler_pokemon)
    RecyclerView pokemonRecyclerView;
    @BindView(R.id.get_photos_button)
    Button getPhotosButton;

    private View view;
    private Context context;

    private PokemonNameAdapter adapter;

    private PublishSubject<Integer> clickSubjects = PublishSubject.create();

    public MainView(MainActivity context){
        this.context = context;
        initView();
    }

    private void initView(){
        FrameLayout parent = new FrameLayout(context);
        parent.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view = LayoutInflater.from(context).inflate(R.layout.activity_main, parent, true);
        ButterKnife.bind(this, view);
        getPhotosButton.setOnClickListener(setListener());
        initRecyclerView();
    }

    private void initRecyclerView(){
        pokemonRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        pokemonRecyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
    }

    private View.OnClickListener setListener(){
        return v -> clickSubjects.onNext(v.getId());
    }

    void disableComponents(){
        progressBar.setVisibility(View.VISIBLE);
        getPhotosButton.setVisibility(View.GONE);
        pokemonRecyclerView.setVisibility(View.GONE);
    }

    void enableComponents(){
        progressBar.setVisibility(View.GONE);
        getPhotosButton.setVisibility(View.VISIBLE);
        pokemonRecyclerView.setVisibility(View.GONE);
    }

    void showToast(String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    View getView(){
        return view;
    }

    Observable<Integer> getClickSubjects(){
        return clickSubjects;
    }

    void fillPokemonList(List<PokemonSimpleObject> results) {
        progressBar.setVisibility(View.GONE);
        getPhotosButton.setVisibility(View.GONE);
        pokemonRecyclerView.setVisibility(View.VISIBLE);
        adapter = new PokemonNameAdapter(context, results);
        pokemonRecyclerView.setAdapter(adapter);
    }
}
