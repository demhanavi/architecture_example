package com.example.architectureexample.screens.main.core;

import com.example.architectureexample.api.ExampleApi;
import com.example.architectureexample.api.dto.PokemonListResponse;
import com.example.architectureexample.persistence.AppDatabase;
import com.example.architectureexample.util.PropertiesReader;
import com.example.architectureexample.util.rx.RxSchedulers;

import io.reactivex.Observable;

public class MainModel {

    private ExampleApi api;
    private PropertiesReader propertiesReader;
    private AppDatabase appDatabase;
    private RxSchedulers schedulers;

    public MainModel(ExampleApi api, PropertiesReader propertiesReader, AppDatabase appDatabase, RxSchedulers schedulers) {
        this.api = api;
        this.propertiesReader = propertiesReader;
        this.appDatabase = appDatabase;
        this.schedulers = schedulers;
    }

    Observable<PokemonListResponse> getPokemon(int id){
        return api.getPokemon(id);
    }
}
