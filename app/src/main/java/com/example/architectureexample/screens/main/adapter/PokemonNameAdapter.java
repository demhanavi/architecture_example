package com.example.architectureexample.screens.main.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.architectureexample.R;
import com.example.architectureexample.api.dto.PokemonSimpleObject;

import java.util.List;

public class PokemonNameAdapter  extends RecyclerView.Adapter<PokemonNameHolder> {

    private Context context;
    private List<PokemonSimpleObject> items;

    public PokemonNameAdapter(Context context, List<PokemonSimpleObject> items){
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public PokemonNameHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_pokemon_name, parent, false);
        return new PokemonNameHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PokemonNameHolder holder, int position) {
        PokemonSimpleObject pokemon = items.get(position);
        holder.setPokemon(pokemon);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

class PokemonNameHolder  extends RecyclerView.ViewHolder {

    private TextView pokemonNameTextView;

    public PokemonNameHolder(@NonNull View itemView) {
        super(itemView);
        pokemonNameTextView = itemView.findViewById(R.id.pokemon_name_text);
    }

    public void setPokemon(PokemonSimpleObject pokemon){
        pokemonNameTextView.setText(pokemon.getName());
    }
}
