package com.example.architectureexample.api.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PokemonListResponse {

    @SerializedName("pokemon_species")
    private List<PokemonSimpleObject> pokemon;

    public List<PokemonSimpleObject> getPokemon() {
        return pokemon;
    }

    public void setPokemon(List<PokemonSimpleObject> pokemon) {
        this.pokemon = pokemon;
    }
}
