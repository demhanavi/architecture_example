package com.example.architectureexample.api;

import com.example.architectureexample.api.dto.PokemonListResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ExampleApi {

    @GET("generation/{id}")
    Observable<PokemonListResponse> getPokemon(@Path("id")int id);
}
